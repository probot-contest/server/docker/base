# probot base image
This image is used when a contestant uploads code for building online

## Installation
```bash
docker build . -t probot/base
```

## Deployment
If the backend runs started with `docker-compose up`:
```bash
DOCKER_HOST="tcp://$(docker inspect probotbackend_docker_1 | node -e 'console.log(JSON.parse(require("fs").readFileSync("/dev/stdin"))[0].NetworkSettings.Networks.probotbackend_default.IPAddress)')" docker build . -t probot/base
```
